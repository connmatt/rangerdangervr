﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleInteract : MonoBehaviour {


    private Toggle toggle;
    // Use this for initialization
    void Start () {
        toggle = transform.GetComponent<Toggle>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "UIInteract")
        {
            toggle.isOn = !toggle.isOn;
        }
    }
}
