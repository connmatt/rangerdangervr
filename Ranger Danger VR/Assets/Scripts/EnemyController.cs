﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    public GameObject enemy;
    public GameObject tnt;
    public bool inSight = false;
    public bool arrowstuff = false;
    public bool slowSpeed = false;
	// Use this for initialization
	void Start () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "tnt")
        {
            inSight = true;
        }
        if(other.gameObject.tag == "arrow")
        {
            arrowstuff = true;
        }
        if(other.gameObject.tag == "oil")
        {
            slowSpeed = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "tnt")
        {
            inSight = false;
        }
        if (other.gameObject.tag == "arrow")
        {
            arrowstuff = false;
        }
        if (other.gameObject.tag == "oil")
        {
            slowSpeed = false;
            //Debug.Log(slowSpeed);
        }
    }
    // Update is called once per frame
    void Update () {
        //if(tnt.GetComponent<TNTTrap>().enemyDied || arrowstuff)
        //{
        //    Debug.Log("DESTROY");
        //    Destroy(this.gameObject);
        //}
    }
}
