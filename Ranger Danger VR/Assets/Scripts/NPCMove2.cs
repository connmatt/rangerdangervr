﻿
using System;
using UnityEngine;
using UnityEngine.AI;

public class NPCMove2 : MonoBehaviour {
    public GameObject enemy;
    public Transform Castle;
    public GameObject tnt;
    private Transform npc;
    public float MinDist = 20.0f;
    public float speed = 2.0f; // Enemy speed
	private WaitForSeconds beforeDestroy = new WaitForSeconds(0.07f);
   	public float smooth = 2.0f;
   	public float tiltAngle = 30.0f;
  	public bool gotShot = false;
  	private Transform Floor;
  	public int positions = 5;
  	Animator anim;
    public GameObject arrow;
    public GameObject oil;
    public bool inSight = false;

	//agent.SetDestination(Player.transform.position);
	// Use this for initialization
	void Start () {
		
        Castle = GameObject.FindWithTag("door").transform;
        tnt = GameObject.FindWithTag("tnt");
        arrow = GameObject.FindWithTag("arrow");
        enemy = GameObject.FindWithTag("enemy");
        oil = GameObject.FindWithTag("oil");
        //speed = 5.0f;

		//npc = GameObject.FindWithTag("NPC").transform;
		//anim = GetComponent<Animator>();
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "tnt")
        {
            inSight = true;
        }
        if(other.gameObject.tag == "oil")
        {
            speed = 1.5f;
        }
        else
        {
            speed = 3;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "oil")
        {
          
            try
            {
                other.GetComponent<NavMeshAgent>().speed = 1.5f;
                Debug.Log(other.GetComponent<NavMeshAgent>().speed);
            }
            catch (Exception e)
            {
               
            }
                
           
        }
        else
        {
            other.GetComponent<NavMeshAgent>().speed = 3;
            Debug.Log(other.GetComponent<NavMeshAgent>().speed);
        }
    }
    //private void OnTriggerStay(Collider other)
    //{
    //    if(other.gameObject.tag == "tnt")
    //    {
    //        inSight = true;
    //    }
    //}
    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.tag == "tnt")
    //    {
    //        inSight = false;
    //    }
    //}
    // Update is called once per frame
    void Update()
    {
        if (!(Castle == null))
        {

            Vector3 npcLookAtPlayer = new Vector3(Castle.position.x, transform.position.y, Castle.position.z);



            //Debug.Log("Arrow: " + tnt.GetComponent<TNTTrap>().arrowActivate);

            if (gotShot == false)
            {
                transform.LookAt(npcLookAtPlayer);
                //if (Vector3.Distance(transform.position, Castle.position) >= 2.0f)
                //{
                //    transform.position += transform.forward * speed * Time.deltaTime;

                //}



            }
            else
            {
                float tiltAroundX = Input.GetAxis("Vertical") * tiltAngle;
                Quaternion target = Quaternion.Euler(tiltAroundX, 0, 0);
                transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
            }
        }


    }
}
