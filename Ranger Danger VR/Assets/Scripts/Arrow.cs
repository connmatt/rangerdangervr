﻿/*
    Copyright (C) 2016 FusionEd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters;

public class Arrow : MonoBehaviour {

	private bool isAttached = false;

    bool triggerpressed = false;

	private bool isFired = false;

    public GameObject tnt;

    int timer;

    LineRenderer trace;

    public GameObject explosion;

    public GameObject[] enemies;

    public GameObject[] traps;


	public GameObject trapToPlace;

    private void Start()
    {
        timer = 1000;
        tnt = GameObject.FindGameObjectWithTag("tnt");

        trace = GetComponent<LineRenderer>();

        trace.enabled = false;

        enemies = GameObject.FindGameObjectsWithTag("enemy");

}

    void OnTriggerStay() {
		AttachArrow ();
	}

	void OnTriggerEnter() {
		AttachArrow ();
	}

	void Update() {


		
		
        

		if (isFired && transform.GetComponent<Rigidbody> ().velocity.magnitude > 5f) {
			transform.LookAt (transform.position + transform.GetComponent<Rigidbody> ().velocity);
		}

        timer--;

        if (timer <= 0)
        {
            Destroy(this.gameObject);
        }

	}

	public void Fired() {

		isFired =  true;
        trace.enabled = true;
	}

	private void AttachArrow() {
		//var device = SteamVR_Controller.Input((int)ArrowManager.Instance.trackedObj.index);
		if (!isAttached && OVRInput.Get(OVRInput.RawButton.RIndexTrigger)) {
			ArrowManager.Instance.AttachBowToArrow ();
			isAttached = true;
		}
	}
    public GameObject Findtnt()
    {
        GameObject[] tnt;
        tnt = GameObject.FindGameObjectsWithTag("tnt");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject t in tnt)
        {
            Vector3 diff = t.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = t;
                distance = curDistance;
            }
        }
        return closest;

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "tnt")
        {
            tnt = Findtnt();
            tnt.GetComponent<TNTTrap>().Activate = true;
            Debug.Log(tnt.GetComponent<TNTTrap>().Activate);
        }
        if(collision.gameObject.tag == "plank")
        {
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "tnt" || collision.gameObject.tag == "enemy")
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
            foreach (GameObject en in enemies)
            {
                Vector3 TnTposition = transform.position;
                Vector3 diff = en.transform.position - TnTposition;
                float compare = diff.sqrMagnitude;
                Debug.Log(compare);
                if (compare < 80.0f)
                {
                    Destroy(en.gameObject);
                }
            }

            Debug.Log("Destroy");
            Destroy(this.gameObject);

        }
        if (collision.transform.tag == "Untagged" || collision.transform.tag == "arrow")
        {

        }
        else
        {
	        GetComponent<Rigidbody>().isKinematic = true;
	        
	        if (trapToPlace != null && collision.transform.tag == "plane")
	        {
                if(trapToPlace != traps[0] && collision.transform.tag == "floor")
                {
                    Instantiate(trapToPlace, transform.position -= new Vector3(0,0.7f,0), Quaternion.identity);
		            Destroy(this.gameObject);
                }
                else
                {
                    Instantiate(trapToPlace, transform.position -= new Vector3(0,0.7f,0), Quaternion.identity);
		            Destroy(this.gameObject);
                }
		        
	        }
	        
            
	       
        }
   
    }
	
	
	


}
