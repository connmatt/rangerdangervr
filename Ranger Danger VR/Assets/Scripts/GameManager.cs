﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameManager : MonoBehaviour {
	public int castleHealth = 100;
    public GameObject enemy;
    public GameObject[] enemies;
    public GameObject tnt;
    public GameObject arrow;
    public GameObject Canvas;
    public bool paused;
    public static bool arrowTracking = true;
    public static int trapcount = -1;
    public Light sceneLight;
    public GameObject player;
    public Transform settingsPos;
    public Transform OtherPil;
    public Transform MainPil;
    public GameObject levelManager;
    bool otherside;
    public AudioSource BGM;
    public AudioSource Sound;

    // Use this for initialization
    void Start () {
        tnt = GameObject.FindWithTag("tnt");
        arrow = GameObject.FindWithTag("arrow");
        Canvas = GameObject.FindWithTag("pause");
        Canvas.gameObject.SetActive(false);
        paused = false;
        pauseGame();
        otherside = false;
        levelManager.SetActive(false);
        //enemy = GameObject.FindWithTag("enemy");

    }
	
	// Update is called once per frame
	void Update () {
        enemies = GameObject.FindGameObjectsWithTag("enemy");
        if (OVRInput.GetDown(OVRInput.Button.Start) == true)
        {
            //paused = !paused;
            pauseGame();


        }
        if(OVRInput.GetDown(OVRInput.Button.Four) == true)
        {
            otherside = !otherside;
            if(otherside == true)
            {
                player.GetComponent<positionChange>().teleportTo(OtherPil);
            }
            else
            {
                player.GetComponent<positionChange>().teleportTo(MainPil);
            }

        }
        if(paused)
        {
            Canvas.gameObject.SetActive(true);
            Time.timeScale = 0;
            Debug.Log(true);
        }
        else
        {
            Canvas.gameObject.SetActive(false);
            Time.timeScale = 1;
        }

      



		//if(tnt.GetComponent<TNTTrap>().arrowActivate)
  //      {
  //          if(enemy.GetComponent<NPCMove2>().inSight)
  //          {
  //              Destroy(enemy);
  //              Destroy(tnt);
  //          }
  //      }
	}

    public void setTracker()
    {
        arrowTracking = !arrowTracking;
    }

    public void changeLight(float value)
    {
        float lightValue = floatToPercent(value) / 100;

        sceneLight.intensity = lightValue;
    }
    public void changeBGM(float value)
    {
        float floatvalue = floatToPercent(value) / 100;
        BGM.volume = floatvalue;
        
    }

    public void changeSound(float value)
    {
        float floatvalue = floatToPercent(value) / 100;
        Sound.volume = floatvalue;

    }


    public float floatToPercent(float v)
    {
        var min = -3;
        var max = -1;
        var returnValue = 100 * (v - min) / (min - max);
        return -returnValue;
    }


    public void pauseGame()
    {
       
        player.GetComponent<positionChange>().teleportTo(settingsPos);
        foreach (GameObject g in enemies)
        {
            Debug.Log("pause");
            // g.GetComponent<NPCMove2>().enabled = !g.GetComponent<NPCMove2>().enabled;
            g.GetComponent<NavMeshAgent>().destination = g.transform.position;


        }
        //levelManager.GetComponent<LevelManager>().enabled = !levelManager.GetComponent<LevelManager>().enabled;

    }
    public void startEnemies()
    {
        foreach (GameObject g in enemies)
        {
            // g.GetComponent<NPCMove2>().enabled = !g.GetComponent<NPCMove2>().enabled;
            g.GetComponent<NavMeshAgent>().isStopped = false;
            g.GetComponent<NavMeshAgent>().speed = 5f;

        }
        // levelManager.GetComponent<LevelManager>().enabled = true;
        
    }
}
