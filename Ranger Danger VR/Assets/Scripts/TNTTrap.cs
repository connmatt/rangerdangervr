﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TNTTrap : MonoBehaviour {
    public GameObject Enemy;
    public GameObject Arrow;
    public AudioSource sound;
    public bool arrowActivate = false;
    //public AudioSource audio;
    public bool enemyInBound = false;
    public bool arrowUsed = false;
    public bool enemyDied = false;
    public bool Activate = false;
    public GameObject[] enemies;
    public AudioClip SOUNDS;
    public float force;
    public GameObject explosion;
    public float radius;

	// Use this for initialization
	void Start () {
        explosion.SetActive(false);
    }


    public void getEnemies()
    {
        Enemy = GameObject.FindGameObjectWithTag("enemy");
        Arrow = GameObject.FindGameObjectWithTag("arrow");
        enemies = GameObject.FindGameObjectsWithTag("enemy");

        Vector3 TnTposition = transform.position;
    }

    public GameObject FindEnemy()
    {
        GameObject[] enm;
        enm = GameObject.FindGameObjectsWithTag("enemy");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach(GameObject e in enm)
        {
            Vector3 diff = e.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if(curDistance < distance)
            {
                closest = e;
                distance = curDistance;
            }
        }
        return closest;

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "arrow")
        {
            arrowActivate = true;
           
            //Arrow = FindArrow();
        }
        if(other.gameObject.tag == "enemy")
        {
            enemyInBound = true;
        }

    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "enemy")
        {
            enemyInBound = true;
            Enemy = FindEnemy();

        }
    }


    // Update is called once per frame
    void Update () {
       // if(!GameObject.Find("Level Manager").GetComponent<LevelManager>().tutorial) enemies = GameObject.FindGameObjectsWithTag("enemy");
        
        if (Activate /*&& enemyInBound*/)
        {
            getEnemies();


            explosion.SetActive(true);
            Instantiate(explosion, transform.position, Quaternion.identity);
            AudioSource audio = GetComponent<AudioSource>();
            sound.Play();
          
            foreach (GameObject en in enemies)
            {
                Vector3 TnTposition = transform.position;
                Vector3 diff = en.transform.position - TnTposition;
                float compare = diff.sqrMagnitude;
                
                if (compare < 80.0f)
                {
                  
                    Destroy(en.gameObject);
                }
            }

            Debug.Log("Destroy");
            Destroy(this.gameObject);

        }
    }
}
