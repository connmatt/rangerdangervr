﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovement : MonoBehaviour {
    private Transform player;
    public float MinDist = 10.0f;
    public float speed = 4.0f; // Enemy speed
	private WaitForSeconds beforeDestroy = new WaitForSeconds(0.07f);
   	public float smooth = 2.0f;
   	public float tiltAngle = 30.0f;
  	public bool gotShot = false;

	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player").transform;
        //npcTransform = transform.position;


    }

    // Update is called once per frame
    void Update()
    {
		Vector3 npcLookAtPlayer  = new Vector3(player.position.x, 0, player.position.z);
		transform.LookAt(npcLookAtPlayer);

    	if(gotShot == false)
    	{
			
		    if (Vector3.Distance(transform.position, player.position) <= MinDist)
		    {
		        transform.position += transform.forward * speed * Time.deltaTime;

		    }
        }
        else
        {
        	float tiltAroundX = Input.GetAxis("Vertical") * tiltAngle;
        	Quaternion target = Quaternion.Euler(tiltAroundX,0,0);
        	transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        }
    }






















    //transform.LookAt(player);
    //if(Vector3.Distance(transform.position,player.position) >= MinDist)
    //{
    //    transform.position += transform.forward * speed * Time.deltaTime;

    //    if(Vector3.Distance(transform.position, player.position) <=MaxDist)
    //    {
    //        Debug.Log("Shoot");
    //    }
    //}
    //Vector3 wantedPosition = new Vector3(player.position.x, transform.position.y, player.position.z);
    //transform.LookAt(player);
    //float distance = Vector3.Distance(transform.position, player.position);
    //if(distance < walkingDistance)
    //{
    //    transform.position = Vector3.SmoothDamp(transform.position, player.position, ref smoothVelocity, smoothTime);
    //}

    //Vector3 currentPos = transform.position;
    //Vector3 targetPos = player.position;
    // Vector3 direction = targetPos - currentPos;
    // currentPos = currentPos + direction;
}
