﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GateHealth : MonoBehaviour {

    // the image inside the canvas
    public Image healthBar;
    // The wall behind the gate.
    public GameObject gateWall;
    //Player transform position.
    public Transform PlayerPos;
   

	

	void Update () {

        // Making the bar look at the player, so that it is easy to see the gate's health.
        healthBar.transform.LookAt(PlayerPos);
        
        // Here I am checking if the health is at 0
        if(healthBar.transform.localScale.x <= 0)
        {
            // Destroying the gate, the wall and the bar.
            Destroy(this.gameObject);
            Destroy(gateWall); // Had to destroy the wall too, because there is no hole through it.
            Destroy(healthBar);
            SceneManager.LoadScene("true game over");


        }
    }




    private void OnCollisionEnter(Collision collision)
    {
        
        if(collision.gameObject.tag == "enemy")
        {
            TakeDamage(0.003f); // here i call the takedamage function to scale down the bar.
            Destroy(collision.gameObject);
        }
        // The float has to be very small because the bar is sized down from a really big scale.
    }


    public void TakeDamage(float damage)
    {
        // Subtracting the localscale with the damage.
        healthBar.transform.localScale -= new Vector3(damage, 0, 0);
       // healthBar.GetComponent<RectTransform>().sizeDelta -= new Vector2(damage, 0);
    }
}
