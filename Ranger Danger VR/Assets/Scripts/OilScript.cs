﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OilScript : MonoBehaviour {
    GameObject enemy;
	// Use this for initialization
	void Start () {
        enemy = GameObject.FindGameObjectWithTag("enemy");
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "enemy")
        {
            other.gameObject.GetComponent<NPCMove2>().speed /= 2.0f;
            other.gameObject.GetComponent<NavMeshAgent>().speed /= 2.0f;
            if (other.gameObject.GetComponent<NPCMove2>().speed < 1.0f)
            {
                other.gameObject.GetComponent<NPCMove2>().speed = 2.5f;
            }
            if (other.gameObject.GetComponent<NavMeshAgent>().speed <= 1.0f)
            {
                other.gameObject.GetComponent<NavMeshAgent>().speed = 2.5f;
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "enemy")
        {
            other.gameObject.GetComponent<NPCMove2>().speed /= 2.0f;
            other.gameObject.GetComponent<NavMeshAgent>().speed /= 2.0f;
            if(other.gameObject.GetComponent<NPCMove2>().speed < 1.0f)
            {
                other.gameObject.GetComponent<NPCMove2>().speed = 2.5f;
            }
            if(other.gameObject.GetComponent<NavMeshAgent>().speed <= 1.0f)
            {
                other.gameObject.GetComponent<NavMeshAgent>().speed = 2.5f;
            }
        }
    }
    private void OnTriggerExit(Collider collision)
    {
        collision.gameObject.GetComponent<NPCMove2>().speed = 5;
        collision.gameObject.GetComponent<NavMeshAgent>().speed = 5;
    }
    // Update is called once per frame
    void Update () {
	}
}
