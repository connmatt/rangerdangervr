﻿/*
    Copyright (C) 2016 FusionEd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using UnityEngine;
using System.Collections;

public class ArrowManager : MonoBehaviour {

	public static ArrowManager Instance;


	//public SteamVR_TrackedObject trackedObj;
	public GameObject trackedObj;

	private GameObject currentArrow;

	public GameObject stringAttachPoint;
	public GameObject arrowStartPoint;
	public GameObject stringStartPoint;

	public GameObject arrowPrefab;

    public GameObject tnt;

	public GameObject bow;
	public GameObject trajectory;
	

    public Vector3 fist;

	public AudioSource audio;

	private bool isAttached = false;

	private GameObject[] traps;
    private Color[] colors = { Color.green, Color.red };
    private Color normalColor;

    private bool clicked = false;

	void Awake() {
		if (Instance == null)
			Instance = this;
	}

	void OnDestroy() {
		if (Instance == this)
			Instance = null;
	}

	// Use this for initialization
	void Start () {
        //tnt = GameObject("tnt");
        //TNTTrap enemy = tnt.GetComponent<TNTTrap>();
        fist = stringAttachPoint.transform.position;

        


    }


    void Update() {
	    
	    
	  
	    if (OVRInput.Get(OVRInput.RawButton.A) || Input.GetKeyDown(KeyCode.A))
	    {
			
            if (!clicked)
            {
                clicked = true;
                GameManager.trapcount++;

                if (GameManager.trapcount >= traps.Length)
                    GameManager.trapcount = -1;

                activateTrapMode(GameManager.trapcount);
            }

        }



        if (OVRInput.GetUp(OVRInput.RawButton.A))
        {
            clicked = false;
        }
  
	
		AttachArrow ();
		PullString ();
        if(tnt.GetComponent<TNTTrap>().arrowUsed)
        {
            Destroy(this.gameObject);
        }
	}



    void activateTrapMode(int count)
    {
        if(count == -1)
        {
            currentArrow.GetComponent<Renderer>().material.color = Color.clear;
            currentArrow.GetComponent<Arrow>().trapToPlace = null;
        }
        else
        {
            currentArrow.GetComponent<Renderer>().material.color = colors[GameManager.trapcount];
            currentArrow.GetComponent<Arrow>().trapToPlace = traps[GameManager.trapcount];
        }
        
    }

	private void PullString() {
		
	
		
		
		
		
		if (isAttached) {

            bow.GetComponent<LineRenderer>().enabled = true;
            float dist = (stringStartPoint.transform.position - trackedObj.transform.position).magnitude;
			Vector3 distt = (stringStartPoint.transform.position - trackedObj.transform.position);
			
			var vel2 = trajectory.transform.forward * 25f * dist;
			
			bow.GetComponent<trajectory>().UpdateTrajectory(trajectory.transform.position, vel2, Physics.gravity);
			
			stringAttachPoint.transform.localPosition = stringStartPoint.transform.localPosition  + new Vector3 (10f* dist, -1.5f, 0f);

			//var device = SteamVR_Controller.Input((int)trackedObj.index);
			var trigger = OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger);
			
			if (!OVRInput.Get(OVRInput.RawButton.RIndexTrigger)) {
				Fire ();
               

            }
		}
	}

	private void Fire() {

		audio.Play();
        bow.GetComponent<LineRenderer>().enabled = false;


		float dist = (stringStartPoint.transform.position - trackedObj.transform.position).magnitude;

		currentArrow.transform.parent = null;
		currentArrow.GetComponent<Arrow> ().Fired ();

		Rigidbody r = currentArrow.GetComponent<Rigidbody> ();
		r.velocity = currentArrow.transform.forward * 25f * dist;
		r.useGravity = true;

		currentArrow.GetComponent<Collider> ().isTrigger = false;

        stringAttachPoint.transform.position = stringStartPoint.transform.position;
        
        currentArrow = null;
		isAttached = false;
	}

	private void AttachArrow() {
		if (currentArrow == null) {
            currentArrow = Instantiate (arrowPrefab);

			traps = currentArrow.GetComponent<Arrow>().traps;
			currentArrow.transform.parent = trackedObj.transform;
			currentArrow.transform.localPosition = new Vector3 (0f, 0f, .342f);
			currentArrow.transform.localRotation = Quaternion.identity;
            activateTrapMode(GameManager.trapcount);

        }
	}

	public void AttachBowToArrow() {
        currentArrow.transform.parent = stringAttachPoint.transform;
        currentArrow.transform.position = arrowStartPoint.transform.position;
        currentArrow.transform.rotation = arrowStartPoint.transform.rotation;
        isAttached = true;
	}
}
