﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    public GameObject enemyPrefab;
    public GameObject enemyPrefab2;
    public GameObject enemyPrefab3;
    public GameObject Player;
    //public GameObject[] enemies;

    float delayTimer;
	public List<GameObject> enemys;

	public Transform[] Spawnpoints;

    public bool tutorial = true;
    int wait = 15;
    RaycastHit hit;

	const int MAX_ATTEMPTS = 100;

    int spawn;
    int spawnPoint;
    float timer = 2000.0f;
    int maxSpawns = 10;
    int mins = 5;
    int seconds = 0;

    public Text gameTimerText;
    float gameTimer = 300f;


    private float waitTimer = 10000;

    
    void Start()
    {
        spawn = 2;
        //enemyPrefab = GameObject.FindGameObjectWithTag("enemy");
			   
    }


   
   

    // Update is called once per frame
    void Update () {

        if (!tutorial)
        {
            gameTimer -= Time.deltaTime;
            int seconds = (int)(gameTimer % 60);
            int minutes = (int)(gameTimer / 60) % 60;

            string timerString = string.Format("{0:0}:{1:00}", minutes, seconds);

            gameTimerText.text = timerString;

            delayTimer = 1f;

            waitTimer += Time.deltaTime;
            //if (gameTimer >= 250 && gameTimer <= 255)
            //{
            //    wait = 25;
            //}
            //else if (gameTimer >= 200 && gameTimer <= 205)
            //{
            //    wait = 20;
            //}
            //else if (gameTimer >= 150 && gameTimer <= 155)
            //{
            //    wait = 15;
            //}
            //else if (gameTimer >= 100 && gameTimer <= 105)
            //{
            //    wait = 10;
            //}
            //else if (gameTimer >= 50 && gameTimer <= 55)
            //{
            //    wait = 5;
            //}
            

            if (waitTimer > wait)
            {
                waitTimer = 0;
                spawn = 2;
                
          
            for (int i = 0; spawn > 0; i++)
            {
                
                    spawnPoint = Random.Range(0, Spawnpoints.Length);
                Debug.Log(spawnPoint);
                Vector3 placement = Spawnpoints[spawnPoint].position;
                if(spawnPoint == 0)
                {
                    enemys.Add(Instantiate(enemyPrefab, placement, Quaternion.identity));
                }
                else if(spawnPoint == 1)
                {
                    enemys.Add(Instantiate(enemyPrefab2, placement, Quaternion.identity));
                }
                else if(spawnPoint == 2)
                {
                    enemys.Add(Instantiate(enemyPrefab3, placement, Quaternion.identity));
                }
               
                spawn--;
            }
            }
            timer--;
            // if(timer <= 0 && maxSpawns > 0)
            // {
            //     spawnPoint = Random.Range(0, Spawnpoints.Length);
            //     Vector3 placement = Spawnpoints[spawnPoint].position;
            //     enemys.Add(Instantiate(enemyPrefab, placement, Quaternion.identity));
            //     timer = 1000;
            //     maxSpawns--;
            // }
            if(gameTimer <= 0)
            {
                SceneManager.LoadScene("Winning scene");
            }
        }
	}

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
 
     // Code to execute after the delay
    }



    //    Died = GameObject.FindWithTag("Player").GetComponent<Patroling>();
    //		//Transform[] tiles = Floor.GetComponentsInChildren<Transform> ();
    //		for (int i = 0; npcs.Count<MAX_NPCs && i<MAX_ATTEMPTS /* && tiles.Count > 0*/; i++) 
    //		{
    //            spawnPoint = Random.Range(0, FloorNotByPlayer.childCount);

    //            //spawnByPlayer = Random.Range(0, FloorbyPlayer.Length);

    //            Vector3 placement = FloorNotByPlayer.GetChild(spawnPoint).position;
    //            if(index == 3)
    //            {
    //                index = 2;
    //            }
    //Vector3 byPlayerPlacement = FloorByPlayer[index].position;

    //bool good_placement = true;

    //			foreach (GameObject npc in npcs) 
    //			{
    //                if (numByPlayer > 0)
    //                {
    //                    Vector3 origin = byPlayerPlacement + new Vector3(0, 1.3f, 0);
    //Vector3 target = npc.transform.position + new Vector3(0, 1.3f, 0);

    //Vector3 direction = target - origin;

    //                    if (Physics.Raycast(origin, direction, out hit, 10))
    //                    {
    //                        if (hit.transform.tag == "Player")
    //                        {
    //                            good_placement = false;
    //                            byPlayer = true;

    //                        }
    //                        //else
    //                        //{
    //                        //    byPlayer = false;
    //                        //    good_placement = true;
    //                        //}
    //                    }
    //                }
    //                else
    //                {
    //                    Vector3 origin = placement + new Vector3(0, 1.3f, 0);

    //Vector3 target = npc.transform.position + new Vector3(0, 1.3f, 0);

    //Vector3 direction = target - origin;

    //                    if (Physics.Raycast(origin, direction, out hit, 200))
    //                    {
    //                        if (hit.transform.tag == NPC_TAG)
    //                        {
    //                            good_placement = false;
    //                            byPlayer = false;

    //                        }
    //                    }
    //                }
    //			}

    //			if (byPlayer && numByPlayer !=0)
    //			{
    //				if (index< 2)
    //				{
    //					npcs.Add(Instantiate (npcGun, byPlayerPlacement, Quaternion.identity));
    //				}
    //				else
    //				{
    //					npcs.Add(Instantiate (npcPrefab, byPlayerPlacement, Quaternion.identity));
    //				}

    //                numByPlayer--;
    //                index++;

    //			}
    //            if(good_placement)
    //            {
    //            	if(ind< 1)
    //            	{
    //					npcs.Add(Instantiate (npcGun, placement, Quaternion.identity));
    //            	}
    //            	else
    //            	{
    //					npcs.Add(Instantiate(npcPrefab, placement, Quaternion.identity));
    //            	}

    //                ind++;

    //            }

    //		}
}
