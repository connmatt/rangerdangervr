﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialBarrel : MonoBehaviour {

	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "arrow")
		{
			Destroy(GameObject.Find("arrowpointer").gameObject);
			GameObject.Find("Level Manager").GetComponent<LevelManager>().tutorial = false;

			foreach (var gm in GameObject.FindGameObjectsWithTag("tnt"))
			{
				gm.GetComponent<TNTTrap>().getEnemies();
			}
			
			foreach (var gm in GameObject.FindGameObjectsWithTag("UI"))
			{
				Destroy(gm);
			}
			
			
			Destroy(this.gameObject);
			
			
		}
	}

}
