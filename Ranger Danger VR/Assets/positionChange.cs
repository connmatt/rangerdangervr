﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class positionChange : MonoBehaviour
{

    private Vector3 initialPos;
    private bool clicked = false;

    public GameManager gm;
	// Use this for initialization
	void Start ()
	{
        initialPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void teleportTo(Transform pos)
    {
        if (!clicked)
        {
            transform.position = pos.position;
        }
        else
        {
            transform.position = initialPos;
        }

        clicked = !clicked;
        
    }


    public void resetPos()
    {
        transform.position = initialPos;
        gm.startEnemies();
        clicked = false;
        
    }
}
