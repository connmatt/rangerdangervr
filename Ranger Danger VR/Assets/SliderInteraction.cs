﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderInteraction : MonoBehaviour {


    private Slider slider;
	// Use this for initialization
	void Start () {
        slider = transform.parent.GetComponent<Slider>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "UIInteract")
        {
            float pos = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position).z;
            slider.value = pos;

        }
    }
    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.tag == "UIInteract")
        {

            float pos = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position).z;
            slider.value = pos;


        }
    }
}
