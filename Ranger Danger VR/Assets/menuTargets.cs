﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menuTargets : MonoBehaviour
{


	public Text left;
    public Text middle;
    public Text right;

    public Transform player;
    public Transform tpPosition;
    private bool shouldReset = false;

    private string[] startStrings = { "Settings", "Play", "Credits" };
    private string[] secondStrings = { "Tutorial", "Start level", "Back" };

    // Use this for initialization
    void Start () {
       if(tpPosition == null)
        {
            shouldReset = true;
           // tpPosition = GameObject.Find("initialPos").transform;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	private void OnCollisionEnter(Collision other)
	{
        if (!shouldReset)
        {
            player.transform.position = tpPosition.position;
        }
        else
        {
            //updateText(secondStrings);
            player.GetComponent<positionChange>().resetPos();
            Destroy(other.gameObject);
        }
            
	}


    private void updateText(string[] stringArray)
    {
        left.text = stringArray[0];
        middle.text = stringArray[1];
        right.text = stringArray[2];
    }


}
