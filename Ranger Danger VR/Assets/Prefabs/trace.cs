﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trace : MonoBehaviour {

    public int traceLength;

    private LineRenderer line;
    private Vector3[] points;
    private int numPoints;

	// Use this for initialization
	void Start () {
        line = transform.GetComponent<LineRenderer>();
        numPoints = 0;
        points = new Vector3[numPoints];
        addPoint();
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.arrowTracking)
            addPoint();
	}

    void addPoint()
    {
        if(numPoints == traceLength)
        {            
            for (int i = 0; i < traceLength - 1; i++)
            {
                points[i] = points[i+1];
            }

            points[traceLength - 1] = transform.position;

            line.positionCount = traceLength;
        }
        else
        {
            Vector3[] newPoints = new Vector3[numPoints + 1];

            for (int i = 0; i < numPoints; i++)
            {
                newPoints[i] = points[i];
            }

            newPoints[numPoints++] = transform.position;

            line.positionCount = numPoints;

            points = newPoints;
        }

        

        line.SetPositions(points);
        //line.Simplify(.3f);
    }

}
